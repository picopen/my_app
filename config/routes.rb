Rails.application.routes.draw do
  namespace :api do
    namespace :v1 do
      resources :ingredients
    end
  end

  resources :recipes, only: [:index, :show] do
    get :search, on: :collection
  end

  root "recipes#index"
end
