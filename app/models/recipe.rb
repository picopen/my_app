# == Schema Information
#
# Table name: recipes
#
#  id              :bigint           not null, primary key
#  budget          :string
#  image_url       :string
#  ingredient_ids  :integer          default([]), not null, is an Array
#  name            :string           not null
#  original_data   :jsonb            not null
#  people_quantity :integer
#  rate            :integer
#  tag_ids         :integer          default([]), is an Array
#  author_id       :bigint
#
# Indexes
#
#  index_recipes_on_author_id  (author_id)
#
class Recipe < ApplicationRecord
  belongs_to :author

  max_paginates_per 10
end
