# == Schema Information
#
# Table name: authors
#
#  id   :bigint           not null, primary key
#  name :string           not null
#
# Indexes
#
#  index_authors_on_name  (name) UNIQUE
#
class Author < ApplicationRecord
end
