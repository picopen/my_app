class RecipesController < ApplicationController
  def index
    @recipes = Recipe.order(id: :desc).
      page(params[:page]).
      includes(:author)
  end

  def show
    @recipe            = Recipe.find(params[:id])
    @ingredients_names = Ingredient.where(id: @recipe.ingredient_ids).pluck(:name)
    @tags_names        = Tag.where(id: @recipe.tag_ids).pluck(:name)
  end

  def search
    people_quantity = begin
                        Integer(search_params["people_quantity"])
                      rescue
                        nil
                      end
    ingredients_ids = search_params["ingredients"]
    return if ingredients_ids.blank?

    @recipes = Recipe.where('ingredient_ids @> ARRAY[?]', ingredients_ids.map(&:to_i))

    if people_quantity.present?
      @recipes = @recipes.where(people_quantity: people_quantity)
    end

    @recipes = @recipes.page(params[:page]).includes(:author)
  end

  private

  def search_params
    params.require(:recipe)
  end
end
