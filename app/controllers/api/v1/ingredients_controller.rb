class Api::V1::IngredientsController < ApplicationController
  def index
    name = params.require(:name)
    @ingredients = Ingredient.select_by_name(name).select(:id, :name)
    render json: @ingredients.pluck(:id, :name)
  end
end
