## Problem statement

> **I am expecting friends for dinner. Knowing what I have in my fridge, what dishes can I cook?**

## Objective

Deliver an application prototype to answer the above problem statement.

By prototype, we mean:

- something usable, yet as simple as possible
- UI / design is not important
- we do not expect features which are outside the basic scope of the problem

We expect to use this prototype as a starting point to discuss current implenentation details, as well as ideas for improvement.

#### Tech must-haves

- [x] MySQL / PostgreSQL or any other MySQL-compatible database.
- [x] A backend application which responds to queries
- [x] A web interface (can be VERY simple)
- [x] Ruby on Rails

#### Bonus points

- [x] Application is hosted on heroku

## User Stories
- As a Regular App User, I need to select a recipe from the the up-to-date list of recipes and get redirected to a specific page, 
which shows more detailed information about the selected recipe.
- As a Regular App User, I need to search for ingredients from a page form, which should display all the available ingredients and their quantity.
- As a Regular App User, I need to search for recipes from a page form, which takes into account the selected ingredients, selected people's quantity and displays me (the user) a list of matched recipes and links to their page.

## Database Structure
![image](db_diagram.png?raw=true)

## The application running on Heroku
https://adrian-recipes-app.herokuapp.com

