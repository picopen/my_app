class CreateTables < ActiveRecord::Migration[6.1]
  def change
    create_table :authors do |t|
      t.string :name, null: false, index: { unique:  true }
    end

    create_table :tags do |t|
      t.string :name, null: false, index: { unique:  true }
      t.timestamps
    end

    create_table :ingredients do |t|
      t.string :name, null: false, index: { unique: true }
      t.timestamps
    end

    create_table :recipes do |t|
      t.belongs_to :author
      t.string :name, null: false
      t.integer :rate
      t.string :budget
      t.integer :people_quantity
      t.string :image_url
      t.integer :tag_ids, array: true, default: []
      t.integer :ingredient_ids, array: true, null: false, default: []
      t.jsonb :original_data,    null: false
    end
  end
end
