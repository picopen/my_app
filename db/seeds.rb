# {
#   "rate": "5",
#   "author_tip": "",
#   "budget": "bon marché",
#   "prep_time": "15 min",
#   "ingredients": ["600g de pâte à crêpe", "1/2 orange", "1/2 banane", "1/2 poire pochée", "1poignée de framboises", "75g de Nutella®", "1poignée de noisettes torréfiées", "1/2poignée d'amandes concassées", "1cuillère à café d'orange confites en dés", "2cuillères à café de noix de coco rapée", "1/2poignée de pistache concassées", "2cuillères à soupe d'amandes effilées"],
#   "name": "6 ingrédients que l’on peut ajouter sur une crêpe au Nutella®",
#   "author": "Nutella",
#   "difficulty": "très facile",
#   "people_quantity": "6",
#   "cook_time": "10 min",
#   "tags": ["Crêpe", "Crêpes sucrées", "Végétarien", "Dessert"],
#   "total_time": "25 min",
#   "image": "https://assets.afcdn.com/recipe/20171006/72810_w420h344c1cx2544cy1696cxt0cyt0cxb5088cyb3392.jpg",
#   "nb_comments": "1"
# }

begin
  File.open("recipes.json", "r").each_line do |line|
    hash = JSON.parse(line)
    
    recipe      = Recipe.new(original_data: hash)
    tags        = Tag.import([:name], hash["tags"].uniq.zip, on_duplicate_key_update: {conflict_target: [:name], columns: {created_at: :updated_at}})
    ingredients = Ingredient.import([:name], hash["ingredients"].uniq.zip, on_duplicate_key_update: {conflict_target: [:name], columns: {created_at: :updated_at}})
    
    recipe.tag_ids         = tags.ids
    recipe.ingredient_ids  = ingredients.ids
    recipe.budget          = hash["budget"]
    image_url              = hash["image"]
    recipe.image_url       = image_url.blank? ? nil : image_url
    recipe.name            = hash["name"]
    recipe.people_quantity = Integer(hash["people_quantity"]) rescue nil
    recipe.rate            = Integer(hash["rate"]) rescue nil
    recipe.author          = Author.find_or_create_by(name: hash["author"])
    recipe.save
  end
end
